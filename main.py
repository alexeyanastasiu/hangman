import time
import click
import os


def parse_dictionary():
    word_list = list()
    try:
        f = open("words.dict", 'r')
    except FileNotFoundError:
        print("Could not find dictionary! Please check if it exists.")
        exit(-1)
    word_list = f.readlines()
    f.close()

    try:
        if not word_list:
            raise ValueError
    except ValueError:
        print("Dictionary is empty or corrupted!")
        exit(-1)

    seed = time.time()
    index = int((seed * 10 - 23 + (23 * 6 * 2002)) % (len(word_list)))
    return str(word_list[index]).strip('\n')


def word(cuv):
    print("Cuvantul are {} litere.".format(len(cuv)))
    cuvv = cuv[0] + "-" * (len(cuv) - 2) + cuv[-1]

    if cuv.count(cuv[0].lower()) > 0:
        primindex = [i for i, letter in enumerate(cuv) if letter == cuv[0].lower()]
        for x in primindex:
            rebut = x + 1
            cuvv = cuvv[0:x] + cuv[0] + cuvv[rebut:]
        return cuvv

    if cuv.count(cuv[-1].lower()) > 0:
        secondindex = [i for i, letter in enumerate(cuv) if letter == cuv[-1].lower()]
        for x in secondindex:
            print(x)
            rebut = x + 1
            cuvv = cuvv[0:x] + cuv[-1] + cuvv[rebut:]
        return cuvv


def afiseaza_om(stadiu, cuvant):
    if stadiu not in range(0, 9):
        exit(-1)
    else:
        stadii = ["______", "|   ( ツ)", "|    |", "|   /|\\", "|    |", "|   / \\", "|  ------", "  |     |"]
        for i in range(0, stadiu):
            print(stadii[i], )
        print(cuvant)


def Character_Check(cvrandom, litera, actual):
    counter = False
    lungime = len(cvrandom)
    for i in range(lungime):
        if cvrandom[i] == litera:
            counter = True
            m = int(i - 1)
            n = int(i + 1)
            actual = actual[0:i] + litera + actual[n:]
    if counter:
        return actual
    else:
        return False


def is_input_ok(litera):
    if isinstance(litera, str):
        user = litera.lower
        user = litera.strip()
    if len(user) > 1:
        print(" Te rugam sa nu trisezi si sa introduci un singur caracter ")
        return False
    else:
        return True


def main():
    copie = parse_dictionary()
    cuvant = word(copie)

    afiseaza_om(0, cuvant)
    stadiu = 0
    while True:
        letter = input("Introdu o litera: ")
        test = is_input_ok(letter)
        if not test:
            continue
        checker = Character_Check(copie, letter, cuvant)
        if not checker:
            stadiu += 1
            print("Gresit! Incearca din nou")
            time.sleep(0.5)
        else:
            cuvant = checker
        click.clear()
        afiseaza_om(stadiu, cuvant)
        if cuvant == copie:
            print("felicitari! ne-ai batut")
            break
        if stadiu == 8:
            print("Ai pierdut, rip")
            break


main()
